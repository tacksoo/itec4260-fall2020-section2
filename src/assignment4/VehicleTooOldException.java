package assignment4;

public class VehicleTooOldException extends RuntimeException {
    public VehicleTooOldException() {
    }

    public VehicleTooOldException(String message) {
        super(message);
    }

    public VehicleTooOldException(String message, Throwable cause) {
        super(message, cause);
    }

    public VehicleTooOldException(Throwable cause) {
        super(cause);
    }
}
