package assignment4;

public class VehicleTooCheapException extends RuntimeException {
    public VehicleTooCheapException() {
    }

    public VehicleTooCheapException(String message) {
        super(message);
    }

    public VehicleTooCheapException(String message, Throwable cause) {
        super(message, cause);
    }

    public VehicleTooCheapException(Throwable cause) {
        super(cause);
    }
}
