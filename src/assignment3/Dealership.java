package assignment3;

import assignment1.Car;
import assignment1.Truck;
import assignment1.Vehicle;
import assignment2.Inventory;
import assignment4.VehicleTooOldException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Dealership {

    private Inventory inventory;

    public Dealership() {
        inventory = new Inventory();
    }

    public int getSize() {
        return inventory.getSize();
    }

    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Populate dealership with vehicles using CSV file from a link
     *
     * @param link CSV file with vehicle information
     */
    public void loadInventoryFromWeb(String link) {
        try {
            URL url = new URL(link);
            InputStream is = url.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                String[] words = line.split(",");
                if (words[0].equals("Car")) {
                    inventory.add(createCarWithLine(line));
                } else if (words[0].equals("Truck")) {
                    inventory.add(createTruckWithLine(line));
                }
            }
            // this is probably a better way to do it :)
            //String str = IOUtils.toString(url.openStream(),"UTF-8");
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Vehicle createTruckWithLine(String line) {
        String[] words = line.split(",");
        String vin = words[1];
        String make = words[2];
        String model = words[3];
        int year = Integer.parseInt(words[4]);
        boolean is4WD = Boolean.getBoolean(words[5]);
        double price = Double.parseDouble(words[6]);
        double mpg = Double.parseDouble(words[7]);
        boolean sideStep = Boolean.parseBoolean(words[8]);
        double tow = Double.parseDouble(words[9]);
        return new Truck(vin, make, model, year, is4WD, price, mpg, sideStep, tow);
    }


    public Vehicle createCarWithLine(String line) {
        String[] words = line.split(",");
        String vin = words[1];
        String make = words[2];
        String model = words[3];
        int year = Integer.parseInt(words[4]);
        boolean is4WD = Boolean.parseBoolean(words[5]);
        double price = Double.parseDouble(words[6]);
        double mpg = Double.parseDouble(words[7]);
        boolean isConvert = Boolean.parseBoolean(words[8]);
        return new Car(vin, make, model, year, is4WD, price, mpg, isConvert);
    }

    public Vehicle createVehicleWithLine(String line) {
        String[] words = line.split(",");
        String vin = words[1];
        String make = words[2];
        String model = words[3];
        int year = Integer.parseInt(words[4]);
        boolean is4WD = Boolean.getBoolean(words[5]);
        double price = Double.parseDouble(words[6]);
        double mpg = Double.parseDouble(words[7]);
        if (words[0].equals("Car")) {
            boolean isConvert = Boolean.parseBoolean(words[8]);
            return new Car(vin, make, model, year, is4WD, price, mpg, isConvert);
        } else {
            boolean sideStep = Boolean.parseBoolean(words[8]);
            double tow = Double.parseDouble(words[9]);
            return new Truck(vin, make, model, year, is4WD, price, mpg, sideStep, tow);
        }
    }


    public Vehicle getCheapestVehicle() {
        return inventory.findCheapestVehicle();
    }

    public Vehicle getMostExpensiveVehicle() {
        return inventory.findMostExpensiveVehicle();
    }
}
