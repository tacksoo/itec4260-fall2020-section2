package assignment2;

import assignment1.Vehicle;
import assignment4.VehicleTooCheapException;
import assignment4.VehicleTooOldException;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

public class Inventory {

    private ArrayList<Vehicle> vehicles;

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public Inventory() {
        vehicles = new ArrayList<>();
    }

    public void add(Vehicle v) {
        if (v.getPrice() < 500) {
            throw new VehicleTooCheapException("Vehicles is less than $500");
        }
        int currentYear = LocalDate.now().getYear();

        if ( currentYear - v.getYear() > 8 ) {
            int years = currentYear - v.getYear();
            throw new VehicleTooOldException("Your vehicle is " + years + " years old");
        }

        if (containsVehicle(v)) {
            throw new IllegalArgumentException("Duplicate vehicle found in inventory");
        }
        vehicles.add(v);
    }


    public boolean containsVehicle(Vehicle v) {
        for (int i = 0; i < vehicles.size(); i++) {
            if (vehicles.get(i).getVin().equals(v.getVin())) {
                return true;
            }
        }
        return false;
    }


    public int getSize() {
        return vehicles.size();
    }

    public void remove(Vehicle v) {
        for (int i = 0; i < vehicles.size(); i++) {
            if (v.getVin().equals(vehicles.get(i).getVin())) {
                vehicles.remove(i);
            }
        }
    }

    /**
     * Get the cheapest vehicle in terms of price
     * if multiple vehicles with the cheapest price exists, the first cheapest is returned
     *
     * @return vehicle with lowest price in inventory
     */
    public Vehicle findCheapestVehicle() {
        if (vehicles.size() == 0) {
            throw new IllegalStateException("No vehicles are in inventory");
        }

        Vehicle cheapestVehicle = vehicles.get(0);
        for (int i = 0; i < vehicles.size(); i++) {
            if (vehicles.get(i).getPrice() < cheapestVehicle.getPrice()) {
                cheapestVehicle = vehicles.get(i);
            }
        }
        return cheapestVehicle;
    }

    // helper
    public double getAveragePriceOfVehicles() {
        return 0;
    }

    public void printAveragePriceOfVehicles() {
        System.out.println(getAveragePriceOfVehicles());
    }

    public Vehicle findMostExpensiveVehicle() {
        return null;
    }
}
