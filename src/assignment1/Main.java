package assignment1;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        /*
        Vehicle mycar = new Vehicle("A12345678", "Genesis", "GV90", 2020, false, 80000, 15);
        mycar.printVehicle();
        Car nicecar = new Car("X123567", "Bugatti", "Veron", 2020, false, 3000000, 5, false);
        nicecar.printVehicle();
        Truck raptor = new Truck("Y12567", "Ford", "Raptor", 2020, false, 50000, 10, true, 5);
        raptor.printVehicle();
         */
        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        for (int i = 0; i < 10; i++) {
            vehicles.add(generateCar());
        }
        for (int i = 0; i < 10; i++) {
            vehicles.get(i).printVehicle();
        }
    }

    public static Car generateCar() {
        String vin = RandomStringUtils.randomAlphanumeric(17).toUpperCase();
        String make = RandomStringUtils.randomAlphabetic(10);
        String model = RandomStringUtils.randomAlphabetic(10);
        int year = RandomUtils.nextInt(1990,2021);
        boolean is4Wheel = RandomUtils.nextBoolean();
        double price = RandomUtils.nextDouble(10000,3000000);
        int mpg = RandomUtils.nextInt(5,50);
        boolean isConvert = RandomUtils.nextBoolean();
        Car c = new Car(vin,make,model,year,is4Wheel,price,mpg,isConvert);
        return c;
    }

    public static Truck generateTruck() {
        String vin = RandomStringUtils.randomAlphanumeric(17).toUpperCase();
        String make = RandomStringUtils.randomAlphabetic(10);
        String model = RandomStringUtils.randomAlphabetic(10);
        int year = RandomUtils.nextInt(1990,2021);
        boolean is4Wheel = RandomUtils.nextBoolean();
        double price = RandomUtils.nextDouble(10000,3000000);
        int mpg = RandomUtils.nextInt(5,50);
        boolean sideStep = RandomUtils.nextBoolean();
        int towCapacity = RandomUtils.nextInt(1,10);
        Truck t = new Truck(vin,make,model,year,is4Wheel,price,mpg,sideStep,towCapacity);
        return t;
    }

}
