package assignment1;

public class Truck extends Vehicle {
    private static final long serialVersionUID = -3941508773972674483L;
    private boolean sideStep;
    private double towCapacity;

    public Truck(String VIN, String make, String model, int modelYear, boolean isFourWheelDrive, double retailPrice, double milesPerGallon, boolean sideStep, double towCapacity) {
        super(VIN, make, model, modelYear, isFourWheelDrive, retailPrice, milesPerGallon);
        this.sideStep = sideStep;
        this.towCapacity = towCapacity;
    }

    public boolean hasSideStep() {
        return sideStep;
    }

    public void setSideStep(boolean sideStep) {
        this.sideStep = sideStep;
    }

    public double getTowCapacity() {
        return towCapacity;
    }

    public void setTowCapacity(double towCapacity) {
        this.towCapacity = towCapacity;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                (sideStep ? "SideStep" : "No SideStep") + "\n" +
                "Capacity " + towCapacity;
    }
}
