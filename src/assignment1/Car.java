package assignment1;

public class Car extends Vehicle {
    private static final long serialVersionUID = -3049763785759783193L;
    private boolean convertible;

    public Car() {}

    public Car(String vin, String make, String model, int year, boolean is4Wheel, double price, double mpg, boolean convertible) {
        super(vin, make, model, year, is4Wheel, price, mpg);
        this.convertible = convertible;
    }

    public boolean isConvertible() {
        return convertible;
    }

    public void setConvertible(boolean convertible) {
        this.convertible = convertible;
    }

    @Override
    public String toString() {
        String c = convertible ? "It is convertible" : "Not convertible";
        return super.toString() + "\n" + c;
    }

    public String getMessage() {
        return "Whee!! This is a cool car!";
    }
}

