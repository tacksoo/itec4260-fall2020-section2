package assignment1;

import java.io.Serializable;

public class Vehicle implements Serializable {
    private static final long serialVersionUID = -3666699028994488694L;

    private String vin;
    private String make;
    private String model;
    private int year;
    private boolean is4Wheel;
    private double price;
    private double mpg;

    public Vehicle() {

    }

    public Vehicle(String vin, String make, String model, int year, boolean is4Wheel, double price, double mpg) {
        this.vin = vin;
        this.make = make;
        this.model = model;
        this.year = year;
        this.is4Wheel = is4Wheel;
        this.price = price;
        this.mpg = mpg;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isIs4Wheel() {
        return is4Wheel;
    }

    public void setIs4Wheel(boolean is4Wheel) {
        this.is4Wheel = is4Wheel;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    /**
     * 2015 Ford F-150
     * 4WD
     * $35,000
     * 17MPG
     *
     * @return String representation of vehicle object
     */
    @Override
    public String toString() {
        String has4WD = is4Wheel ? "4WD" : "No 4WD";
        return year + " " + make + " " + model + "\n" +
               has4WD + "\n" + "$" + price + "\n" +
               mpg + "MPG";
    }

    /**
     * Prints the vehicle object
     */
    public void printVehicle() {
        System.out.println(this);
    }
}
