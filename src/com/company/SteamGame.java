package com.company;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SteamGame {
    private String title;
    private double salePrice;
    private String steamRatingText;

    public SteamGame() {
    }

    public SteamGame(String title, double salePrice, String steamRatingText) {
        this.title = title;
        this.salePrice = salePrice;
        this.steamRatingText = steamRatingText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public String getSteamRatingText() {
        return steamRatingText;
    }

    public void setSteamRatingText(String steamRatingText) {
        this.steamRatingText = steamRatingText;
    }
}
