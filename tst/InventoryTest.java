import assignment1.Car;
import assignment1.Main;
import assignment1.Vehicle;
import assignment2.Inventory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class InventoryTest {

    @Test
    public void testInventory() {
        Inventory i = new Inventory();
        Assert.assertNotNull(i);
        // tdd in a nutshell
        // 1. write test first
        // 2. make the test fail
        // 3. write just enough code to pass the test
        // 4. repeat until you finish project :)
    }

    @Test
    public void testAddToInventory() {
        Inventory inv = new Inventory();
        Vehicle v = Main.generateTruck();
        inv.add(v);
        Assert.assertEquals(1, inv.getSize());
        //inv.add(v);
        //Assert.assertEquals(1, inv.getSize());
        Vehicle t = Main.generateTruck();
        inv.add(t);
        Assert.assertEquals(2, inv.getSize());
    }

    @Test
    public void testContains() {
        Inventory inv = new Inventory();
        Vehicle v = Main.generateCar();
        inv.add(v);
        Assert.assertTrue(inv.containsVehicle(v));
        Vehicle t = Main.generateTruck();
        Assert.assertFalse(inv.containsVehicle(t));
    }

    @Test
    public void testRemove() {
        Inventory inv = new Inventory();
        Vehicle v = Main.generateCar();
        inv.add(v);
        inv.remove(v);
        Assert.assertEquals(0, inv.getSize());
    }

    @Test
    public void testCheapest() {
        Inventory inv = new Inventory();
        Vehicle c1 = Main.generateCar();
        Vehicle c2 = Main.generateCar();
        Vehicle t1 = Main.generateTruck();
        inv.add(c1);
        inv.add(c2);
        inv.add(t1);
        double cheapest = Math.min(Math.min(c1.getPrice(), c2.getPrice()), t1.getPrice());
        Assert.assertEquals(cheapest, inv.findCheapestVehicle().getPrice(), 0.1);
    }

    @Test
    public void testAveragePrice() {
        Inventory inv = new Inventory();
        Vehicle c1 = Main.generateCar();
        Vehicle c2 = Main.generateCar();
        Vehicle t1 = Main.generateTruck();
        inv.add(c1);
        inv.add(c2);
        inv.add(t1);
        double average = (c1.getPrice() + c2.getPrice() + t1.getPrice()) / 3.0;
        //Assert.assertEquals(average,inv.getAvergePriceOfVehicles(),0.1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDuplicateVehicle() {
        Inventory inv = new Inventory();
        Car c = (Car) Main.generateCar();
        Car duplicate = new Car(c.getVin(), c.getMake(), c.getModel(), c.getYear(), c.isIs4Wheel(), c.getPrice(), c.getMpg(), c.isConvertible());
        inv.add(c);
        inv.add(duplicate);
    }


}
