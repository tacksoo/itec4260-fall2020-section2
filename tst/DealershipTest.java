import assignment1.Car;
import assignment1.Truck;
import assignment1.Vehicle;
import assignment2.Inventory;
import assignment3.Dealership;
import assignment4.VehicleTooOldException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.*;

import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Date;

public class DealershipTest {

    private static final String CSV_LINK = "https://gist.githubusercontent.com/tacksoo/260dc1d2e6c3bf11186aa02cde72693a/raw/9a91b102eb21589dd4c99ceb26cbd4a0f362671a/vehicles.csv";
    private Dealership d;
    private static PrintStream stdOut;

    @BeforeClass
    public static void prepareFixture() throws IOException {
        stdOut = System.out;
        System.setOut(new PrintStream("log.json"));
    }

    @Before
    public void setUp() {
        d = new Dealership();
    }

    @Test
    public void emptyDealerTest() {
        Assert.assertEquals(0, d.getSize());
    }

    @Test
    public void loadFromWebTest() {
        d.loadInventoryFromWeb(CSV_LINK);
        Assert.assertEquals(4, d.getSize());
        System.out.println("testing loadFromWeb");
    }

    @Test
    public void testCheapestVehicleInDealership() {
        d.loadInventoryFromWeb(CSV_LINK);
        Vehicle cheap = d.getCheapestVehicle();
        Assert.assertEquals("Viper", cheap.getModel());
        System.out.println("testCheapestVehicle");
    }

    @Test
    public void testMostExpensiveVehicleInDealership() {
        d.loadInventoryFromWeb(CSV_LINK);
        Vehicle expensive = d.getMostExpensiveVehicle();
        Assert.assertEquals("F-150",expensive.getModel());
    }

    @Test
    public void testAveragePriceOfDealership() {
        d.loadInventoryFromWeb(CSV_LINK);
        //  ( 50000 + 90000 + 20000 + 50000  ) / 4.0

    }

    @Test
    public void testCurrentYear() {
        Date now = new Date();
        now.toString();
        System.out.println(now);
        String year = now.toString().split(" ")[5];
        System.out.println(year);
        //Assert.assertEquals(Integer.parseInt(year),2020);

        LocalDate date = LocalDate.now();
        System.out.println(date.getYear());


    }

    @Test(expected = VehicleTooOldException.class)
    public void testVehicleTooOld() {
        Vehicle car = new Vehicle("XYZ","Kia","Rio", 2005,false, 5000, 40);
        Inventory inv = new Inventory();
        inv.add(car);
    }

    @Test
    public void testCreateCarWithLine() {
        String line = "Car,A12345,Kia,Stinger,2020,true,50000,15,true\n";
        Car car = (Car) d.createCarWithLine(line);
        Assert.assertEquals("A12345", car.getVin());
        Assert.assertEquals("Stinger", car.getModel());
        Assert.assertEquals(false, car.isConvertible());
    }

    @Test
    public void testCreateTruckWithLine() {
        String line = "Truck,N12345,Tesla,Cybertruck,2022,false,50000,80,true,10\n";
        Truck truck = (Truck) d.createTruckWithLine(line);
        Assert.assertEquals("N12345", truck.getVin());
        Assert.assertEquals(10, truck.getTowCapacity(), 0);
        Assert.assertEquals(true, truck.hasSideStep());
    }

    @Test(expected = ArithmeticException.class,timeout = 10)
    public void testExceptions() {
        System.out.println(0 / 0);
    }

    @Test(timeout = 5000)
    public void testLoop() {
        while(true) {}
    }

    @AfterClass
    public static void cleanUpFixture() {
        System.setOut(stdOut);
        System.out.println("Cleaned up!");
    }

    @Test
    public void testStoreToJSON() throws IOException {
        String link = "https://gist.githubusercontent.com/tacksoo/ae7f9255b1b6d9952fe988f5cd519cea/raw/0b95b2b10593d9174e891935de3d930fc5d69213/cars.csv";
        Dealership dealer = new Dealership();
        dealer.loadInventoryFromWeb(link);
        Assert.assertEquals(3,dealer.getSize());
        ObjectMapper mapper = new ObjectMapper();
        Inventory inv = dealer.getInventory();
        String json = mapper.writeValueAsString(inv);
        System.out.println(json);
    }


}
