import com.company.Greeting;
import com.company.SteamGame;
import com.company.Weather;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MidtermTest {

    @Test
    public void testHighestIDGreeting() {
        List<Greeting> greetings = new ArrayList<>();
        Assert.assertNull(getHighestGreeting(greetings));
        greetings.add(new Greeting(7, "hello"));
        greetings.add(new Greeting(10, "hi"));
        greetings.add(new Greeting(678, "yay"));
        greetings.add(new Greeting(3, "hola"));
        greetings.add(new Greeting(0, "hihi"));
        Assert.assertEquals(678, getHighestGreeting(greetings).getId());
    }

    public Greeting getHighestGreeting(List<Greeting> list) {
        if (list.size() == 0) {
            return null;
        }
        Greeting highest = list.get(0);
        for (int i = 0; i < list.size(); i++) {
            if (highest.getId() < list.get(i).getId()) {
                highest = list.get(i);
            }
        }
        return highest;
    }

    public ArrayList<Double> getPrices(String url) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SteamGame[] games = mapper.readValue(new URL(url), SteamGame[].class);
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < games.length; i++) {
            prices.add(games[i].getSalePrice());
        }
        return prices;
    }

    @Test
    public void testGamePrices() throws IOException {
        String link = "https://www.cheapshark.com/api/1.0/deals?storeID=1&metacritic=80&onSale=1";
        ArrayList<Double> prices = getPrices(link);
        System.out.println(prices);
        for (int i = 0; i < prices.size(); i++) {
            Assert.assertTrue(prices.get(i) >= 0);
        }
    }

    @Test
    public void testGamePricesWithJsonNode() throws IOException {
        String link = "https://www.cheapshark.com/api/1.0/deals?storeID=1&metacritic=80&onSale=1";
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(new URL(link));
        //System.out.println(root.size());
        ArrayList<Double> prices = new ArrayList<>();
        for (int i = 0; i < root.size(); i++) {
            prices.add(root.get(i).get("salePrice").asDouble());
        }
        for (int i = 0; i < prices.size(); i++) {
            Assert.assertTrue(prices.get(i) >= 0);
        }
    }

    @Test
    public void testWeatherObject() throws IOException {
        String link = "https://api.darksky.net/forecast/3c5084c558861c1610447b49a45f4eb4/37.8267,-122.4233";
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(new URL(link));
        double latitude = root.get("latitude").asDouble();
        double longitude = root.get("longitude").asDouble();
        double temperature = root.get("currently").get("temperature").asDouble();
        Weather drimsOffice = new Weather(latitude, longitude, temperature);
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(drimsOffice);
        System.out.println(json);
        Assert.assertFalse(json.isEmpty());
        Assert.assertTrue(json.indexOf("latitude") != -1);
        Assert.assertTrue(json.indexOf("longitude") != -1);
        Assert.assertTrue(json.indexOf("temperature") != -1);
    }

    @Test
    public void testSelenium() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.ggc.edu");
    }
}
